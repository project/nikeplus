<?php

class views_plugin_style_nikeplus extends views_plugin_style {
  
  /**
   * Set default options
   */
  function options(&$options) {
    $options['type'] = 'bar';
    $options['value'] = 'distance';
    $options['width'] = '1000';
  }
  
  /**
   * Provide a form for setting options.
   *
   * @param array $form
   * @param array $form_state
   */  
  function options_form(&$form, &$form_state) {
    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('Chart Type'),
      '#options' => array('bar' => t('Bar'), 'line' => t('Line')),
      '#default_value' => $this->options['type'],
    );
    $form['value'] = array(
      '#type' => 'radios',
      '#title' => t('Chart Values'),
      '#options' => array('distance' => t('Distance'), 'pace' => t('Pace')),
      '#default_value' => $this->options['value'],
    );
    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Chart Width'),
      '#default_value' => $this->options['width'],
      '#description' => t('Maximum 1000'),
    );
  }

  function render() {
    $rows = array();
    foreach ($this->view->result as $row) {
      // @todo: Include separator as an option.
      $rows[] = $row;
    }
    return theme($this->theme_functions(), $this->view, $this->options, $rows);
  }
}


