<?php
// $Id
/**
 * @file views_handler_field_nikeplus_distance.inc
 *
 * A handler to properly format nikeplus distance values.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_nikeplus_distance extends views_handler_field_numeric {
  function option_definition() {
    $options = parent::option_definition();

    $options['display_as_kilometers'] = array('default' => FALSE);

    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['display_as_kilometers'] = array(
      '#title' => t('Display distance in kilometers'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['display_as_kilometers']),
    );
  }  

  function render($values) {
    $value = $values->{$this->field_alias};
    $format = $this->options['display_as_kilometers'];
    if ($format == FALSE) {
      $distance = round(($value  / 1.60945), 2) ." mi";
    } 
    else {
      $distance = $value ." km";
    }
    return ($distance);
  }
}
