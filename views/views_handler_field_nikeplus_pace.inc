<?php
// $Id
/**
 * A handler to calculate pace from duration and distance.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_nikeplus_pace extends views_handler_field_numeric {
  function construct() {
    parent::construct();
    $this->additional_fields['duration'] = 'duration';
    $this->additional_fields['distance'] = 'distance';
  }

  function query() {
    $this->add_additional_fields();
  }
  
  function render($values) {
    //$distance = $values->distance;
    //$duration = $values->duration;
    $du = $values->{$this->aliases['duration']};
    $di = $values->{$this->aliases['distance']};
    $pace = get_pace($di, $du, "mi");
    
    
    return ($pace);
  }
}

/**
 * Calculate the pace of a run
 *
 * @param $dist
 *   A float containing distance
 * @param $dur
 *   An integer containing duration in seconds
 * @param $unit
 *   A string containing the units to display.  Defaults to "mi"
 * @return
 *   A string of the pace formated as hh:mm:ss
 */
function get_pace($dist, $dur, $unit="mi") {
    if (!$dist) return "";
    if ($unit=="mi") $dist = (double)$dist/1.609344;
    else $dist = (double)$dist;
    $pace = @(($dur/1000) / $dist);
    $min = (int)($pace/60);
    $sec = $pace%60 ;
    return sprintf("%2d'%02d\"", $min, $sec);
}