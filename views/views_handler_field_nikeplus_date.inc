<?php
// $Id
/**
 * @file views_handler_field_nikeplus_date.inc
 *
 * A handler to properly format nikeplus date display
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_nikeplus_date extends views_handler_field_date {
  function render($values) {
    $value = $values->{$this->field_alias};
    $format = $this->options['date_format'];
    if ($format == 'custom' || $format == 'time ago') {
      $custom_format = $this->options['custom_date_format'];
    }
    
    $nikeplus_date = substr($value, 0, 10);
    $nikeplus_time = substr($value, 11, 8);
    
    return ($nikeplus_date .' '. $nikeplus_time);
  }
}
