<?php
// $Id
/**
 * @file views_handler_nikeplus_duration.inc
 *
 * A handler to properly format run duration values.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_nikeplus_duration extends views_handler_field_numeric {
  function render($values) {
    $value = $values->{$this->field_alias};
    $dur = get_duration($value); 
    return ($dur);
  }
}

/**
 * Calculate the duration of a run
 *
 * @param $dur
 *   An integer containing duration in seconds
 * @return
 *   A string containing duration in mm:ss
 */
function get_duration($dur) {
  $hours   = (int)($dur/3600000);
  $minutes = (int)(($dur%3600000)/60000);
  $seconds = (int)((($dur%3600000)%60000)/1000);
  if ($hours) return sprintf("%2d:%02d'%02d\"", $hours, $minutes, $seconds);
  else return sprintf("%2d'%02d\"", $minutes, $seconds);
}