
The nikeplus module allows users to specify their Nike+ username/password 
to provide a block that displays their NikePlus totals.  In addition, the
module will download run data periodically (hourly, daily or weekly).  
Views are created to view this data.

Installation
------------
Copy nikeplus to your module directory (usually sites/all/modules)
and then enable on the admin modules page.  Settings for the NikePlus
module can be found at admin > settings > nikeplus.

Requirements
------------
Drupal 6.x and PHP 5.1.  NikePlus uses the SimpleXML object.  
Views 6.x-2.0-rc5 is required to view individual runs.
NikePlus relies on cron to sync data from the nikeplus.com website.
If cron isn't enabled, or isn't working, you can run cron manually.

Author
------
Steve Karsch
steve@stevekarsch.com