<?php

/**
 * @file views-view-nikeplus.tpl.php
 *
 * Displays a chart based on nikeplus data
 *
 * A views template file which takes the $row output from Views and
 * constructs a Google Charts API URL to display a chart of run data.
 */

$url = 'http://chart.apis.google.com/chart?';
$width = $view->style_plugin->options['width'];
$title = $view->style_plugin->options['value'];
$height = round($width / 3.34);

if ($view->style_plugin->options['type'] == 'line') {

  $run = split(",", $rows[0]->nikeplus_extended_data);
  $intervals = array();
  for ($i = 0; $i < sizeof($run); $i++) {
    if ($i == 0) {
      array_push($intervals, 0);
    } else {
      array_push($intervals, round($run[$i] - $run[$i - 1], 8) * 2000);
    }    
  }
  
  $url .= "chs=" . $width . "x" . $height . "&cht=lc&chd=t:" . implode(",", $intervals);
  echo '<img src="' . $url . '" />';
  
  
} else {
  $array_values = array();
  $array_day = array();
  foreach ($rows as $row) {
    if ($title == 'distance') {
      array_push($array_values, round(($row->nikeplus_distance  / 1.60945), 2));
    } else {
      $pace = _nikeplus_pace($row->nikeplus_distance, $row->nikeplus_duration);
      $pace = split('\'', $pace);
      $mins = $pace[0];
      $secs = $pace[1];
      $pace = $mins . '.' . (round($secs / 60, 2) * 100);
     
      array_push($array_values, trim($pace));
    }
    array_push($array_day, $row->nikeplus_start_time);
  }
  $chbh = round($width / count($rows)) - 8;
  $url .= 'chtt=' . t(ucwords($title)) . '&chbh=' . $chbh . ',10,10&chs=' . $width . 'x' . $height;
  
  $url .= '&cht=bvs';
  
  $url .= '&chd=t:' . implode($array_values, ",");
  
  $url .= '&chxt=x,y';
  
  $num_days = sizeof($array_values);
  
  $max_value = round(max($array_values)) + 1;
  
  $days = array();
  $value = array();
  
  for ($i = 0; $i < sizeof($array_day); $i++) {
    array_push($days, substr($array_day[$i], 5, 5));
  }
  
  for ($i = 0; $i < $max_value; $i++) {
    array_push($value, $i);
  }
  
  $url .= '&chds=0,' . (round($max_value));
  
  $url .= '&chxl=0:|' . implode($days, "|") . '|1:|' . implode($value, '|') . '|';
  
  
  $output .= '<p><img class="nikeplus-chart" src="' . $url . '" /></p>';
  
  echo $output;
}
?>
