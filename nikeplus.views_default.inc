<?php

/**
 * @file nikeplus.views_default.inc
 *
 * Provide views of nikeplus run data
 *
 * Three views are provided out of the box: a page view that lists
 * all runs sorted by date descending (with a Google chart attached), and a
 * block view that displays the data from the most recent run.
 */

/**
 * Implementation of hook_default_view_views().
 */
function nikeplus_views_default_views() {
    $view = new view;
    $view->name = 'nikeplus';
    $view->description = 'Displays run data downloaded from the Nike+ website.';
    $view->tag = 'default';
    $view->view_php = '';
    $view->base_table = 'nikeplus';
    $view->is_cacheable = FALSE;
    $view->api_version = 2;
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
    $handler = $view->new_display('default', 'Defaults', 'default');
    $handler->override_option('fields', array(
      'start_time' => array(
        'label' => 'Run Start',
        'date_format' => 'small',
        'custom_date_format' => '',
        'exclude' => 0,
        'id' => 'start_time',
        'table' => 'nikeplus',
        'field' => 'start_time',
        'relationship' => 'none',
      ),
      'distance' => array(
        'label' => 'Distance',
        'set_precision' => 0,
        'precision' => '0',
        'decimal' => '.',
        'separator' => ',',
        'prefix' => '',
        'suffix' => '',
        'display_as_kilometers' => 0,
        'exclude' => 0,
        'id' => 'distance',
        'table' => 'nikeplus',
        'field' => 'distance',
        'relationship' => 'none',
      ),
      'duration' => array(
        'label' => 'Duration',
        'set_precision' => FALSE,
        'precision' => 0,
        'decimal' => '.',
        'separator' => ',',
        'prefix' => '',
        'suffix' => '',
        'exclude' => 0,
        'id' => 'duration',
        'table' => 'nikeplus',
        'field' => 'duration',
        'relationship' => 'none',
      ),
      'pace' => array(
        'label' => 'Pace',
        'set_precision' => FALSE,
        'precision' => 0,
        'decimal' => '.',
        'separator' => ',',
        'prefix' => '',
        'suffix' => '',
        'exclude' => 0,
        'id' => 'pace',
        'table' => 'nikeplus',
        'field' => 'pace',
        'relationship' => 'none',
      ),
      'calories' => array(
        'label' => 'Calories',
        'set_precision' => FALSE,
        'precision' => 0,
        'decimal' => '.',
        'separator' => ',',
        'prefix' => '',
        'suffix' => '',
        'exclude' => 0,
        'id' => 'calories',
        'table' => 'nikeplus',
        'field' => 'calories',
        'relationship' => 'none',
      ),
    ));
    $handler->override_option('sorts', array(
      'start_time' => array(
        'order' => 'DESC',
        'granularity' => 'second',
        'id' => 'start_time',
        'table' => 'nikeplus',
        'field' => 'start_time',
        'relationship' => 'none',
      ),
    ));
    $handler->override_option('access', array(
      'type' => 'none',
    ));
    $handler->override_option('cache', array(
      'type' => 'none',
    ));
    $handler->override_option('use_pager', '1');
    $handler->override_option('style_plugin', 'table');
    $handler = $view->new_display('page', 'Page', 'page_1');
    $handler->override_option('title', 'My Nike+ Runs');
    $handler->override_option('path', 'nikeplus');
    $handler->override_option('menu', array(
      'type' => 'none',
      'title' => '',
      'description' => '',
      'weight' => 0,
      'name' => 'navigation',
    ));
    $handler->override_option('tab_options', array(
      'type' => 'none',
      'title' => '',
      'description' => '',
      'weight' => 0,
      'name' => 'navigation',
    ));
    $handler = $view->new_display('block', 'Most Recent Run', 'block_1');
    $handler->override_option('title', 'Most Recent Run');
    $handler->override_option('items_per_page', 1);
    $handler->override_option('use_pager', '0');
    $handler->override_option('use_more', 1);
    $handler->override_option('style_plugin', 'list');
    $handler->override_option('block_description', '');
    $handler->override_option('block_caching', -1);
    $handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
    $handler->override_option('title', 'Chart');
    $handler->override_option('style_plugin', 'views_nikeplus');
    $handler->override_option('style_options', array(
      'type' => 'bar',
      'value' => 'distance',
      'width' => '1000',
    ));
    $handler->override_option('attachment_position', 'after');
    $handler->override_option('inherit_arguments', TRUE);
    $handler->override_option('inherit_exposed_filters', FALSE);
    $handler->override_option('inherit_pager', 1);
    $handler->override_option('render_pager', 0);
    $handler->override_option('displays', array(
      'page_1' => 'page_1',
      'default' => 0,
      'block_1' => 0,
    ));


  $views[$view->name] = $view;

  return $views;
}

