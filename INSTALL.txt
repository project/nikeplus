/* $Id */

Installation
------------
Copy nikeplus to your module directory (usually sites/all/modules)
and then enable on the admin modules page.  You will need to enter
the username and password that you use to access nikeplus.com on
admin > site configuration > nikeplus module settings.  You can 
also set how often you'd like your data sync'd (hourly, daily, weekly).
In order to perform the initial sync of your data, run cron.