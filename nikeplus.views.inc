<?php

/**
 * @file nikeplus.views.inc
 *
 * Provides nikeplus data to views
 *
 * This file describes the nikeplus table to the views module so that
 * views can be created based on nikeplus data.
 */

function nikeplus_views_data() {
  // ----------------------------------------------------------------
  // nikeplus table

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['nikeplus']['table']['group']  = t('Nikeplus');
  $data['nikeplus']['table']['base'] = array(
    'field' => 'run_id',
    'title' => t('NikePlus'),
    'help' => t('A NikePlus run'),
  );

  // date/time of run start
  $data['nikeplus']['start_time'] = array(
    'title' => t('Run Start'),
    'help' => t("The start time/date of a run."), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_nikeplus_date',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
  // run distance
  $data['nikeplus']['distance'] = array(
    'title' => t('Distance'),
    'help' => t("The distance of a run."),
    'field' => array(
      'handler' => 'views_handler_field_nikeplus_distance',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
  );
  // run duration - Duration is stored in seconds, a views handler is defined to convert it to to hh:mm:ss
  $data['nikeplus']['duration'] = array(
    'title' => t('Duration'),
    'help' => t('The duration of a run.'),
    'field' => array(
      'handler' => 'views_handler_field_nikeplus_duration',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
  );
  // pace of the run
  $data['nikeplus']['pace'] = array(
    'title' => t('Pace'),
    'help' => t('The pace of a run.'),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',  
    ),  
    'field' => array(
      'handler' => 'views_handler_field_nikeplus_pace',
      'click sortable' => TRUE,
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
    ),
  );
  // calories burned during the run
  $data['nikeplus']['calories'] = array(
    'title' => t('Calories'),
    'help' => t('Amount of calories burned.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
  );
  return $data;
}

// define custom views handlers
function nikeplus_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'nikeplus') .'/views',
    ),
    'handlers' => array(
      'views_handler_field_nikeplus_date' => array(
        'parent' => 'views_handler_field_date',
      ),
      'views_handler_field_nikeplus_distance' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'views_handler_field_nikeplus_duration' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'views_handler_field_nikeplus_pace' => array(
        'parent' => 'views_handler_field_numeric',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_plugin
 *
 */
function nikeplus_views_plugins() {
  return array(
    'style' => array( //declare the nikeplus_* style plugins
      'views_nikeplus' => array(
        'title' => t('NikePlus Chart'),
        'help' => t('Displays rows in a chart.'),
        'handler' => 'views_plugin_style_nikeplus',
        'theme' => 'views_view_nikeplus',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'help topic' => 'style-nikeplus',
      ),
    ),
  );
}